<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'id_stripe',
        'amount_stripe',
        'amount_platform',
        'type_card',
        'status',
        'last4',
        'seller_message',
        'receipt_url_stripe',
        'idUser'
    ];

        protected $casts = [
        'created_at' => 'datetime:d/m/Y',
    ];

}
