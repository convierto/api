<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplatesMessages extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'idTemplate',
        'message',
        'required',
    ];
}
