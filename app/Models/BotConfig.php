<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotConfig extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'uuidUser',
        'name',
        'welcome_message',
        'goodbye_message',
        'status'
    ];
}
