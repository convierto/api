<?php

namespace App\Http\Controllers;


use App\Models\{TemplatesMessages, Templates};
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TemplatesMessagesController extends Controller
{
    public function index($template)
    {
        $template = Templates::whereUuid($template)->first();
        $messages = TemplatesMessages::where('idTemplate', $template->id)->get();

        return response()->json([
            'data' => $messages,
            'message' => 'Mensajes Obtenidos con Exito',
            'status' => 200,
        ]);
    }

    
    public function store(Request $request)
    {
        $template = Templates::whereUuid($request->idTemplate)->first();

        $message = TemplatesMessages::create([
            'uuid' => Str::uuid(),
            'idTemplate' => $template->id,
            'message' => $request->message,
            'required' => $request->required,
        ]);

        return response()->json([
            'data' => $message,
            'message' => 'Mensaje Guardado con Exito',
            'status' => 200,
            'uuid_template' => $template->uuid
        ]);
    }

    public function update(Request $request, $message)
    {
        if($request->required == null)
        {
            $request->required = 0;
        }

        $message = TemplatesMessages::whereUuid($message)->first();
        $message->message = $request->message;
        $message->required = $request->required;
        $message->save();

        $template = Templates::find($message->idTemplate);

        return response()->json([
            'data' => $message,
            'message' => 'Mensaje Actualizado con Exito',
            'status' => 200,
            'id_template' => $template->uuid
        ]);
    }

    public function delete($messages)
    {
        $message = TemplatesMessages::whereUuid($messages)->first();

        $message->delete();

        return response()->json([
            'data' => $message,
            'message' => 'Mensaje Eliminado con Exito',
            'status' => 200,
        ]);
    }

    public function show($message)
    {
        $message = TemplatesMessages::whereUuid($message)->first();

        return response()->json([
            'data' => $message,
            'message' => 'Mensaje Obtenido con Exito',
            'status' => 200,
        ]);
    }
}
