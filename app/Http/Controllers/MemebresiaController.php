<?php

namespace App\Http\Controllers;

use App\Models\{Memebresia, User};
use Illuminate\Support\Facades\Auth;

class MemebresiaController extends Controller
{

    public function show($user)
    {
        $user = User::whereUuid($user)->first();
        $membresias = Memebresia::where('idUser', $user->id)->with('detallePaquete', 'detallePago')->get();

        return response()->json([
            'data' => $membresias,
            'message' => 'Membresias Obtenidas con Exito',
            'status' => 200
        ]);
    }

}
