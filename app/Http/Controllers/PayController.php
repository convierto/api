<?php

namespace App\Http\Controllers;

use App\Models\Memebresia;
use App\Models\Paquetes;
use App\Models\Pay;
use App\Models\User;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class PayController extends Controller
{
    public function index()
    {
        $pays = Pay::all();

        return response()->json([
            'data' => $pays,
            'message' => 'Pagos Obtenidos con Exito',
            'status' => 200
        ]);
    }


    public function paymentProcess($package, $usuario)
    {
        $user = User::whereUuid($usuario)->first();
        $package = Paquetes::whereUuid($package)->first();
        \Stripe\Stripe::setApiKey(
            'sk_test_51MRivTEigSsp7sX62Cj4CJZ2sAeWjBCvCYLDIjp8rI05HZBnPHwwrHKXvGnegD3ah3CsW4S81O5pr8nZw9tXXkhq00FTH700ll'
        );

        $token = $_POST['stripeToken'];


        try {
            $charge = \Stripe\Charge::create([
                'amount' => $package->price * 100,
                'currency' => 'usd',
                'description' => $package->title,
                'source' => $token,
            ]);
          } catch(\Stripe\Exception\CardException $e) {

            return redirect()->route('pay-reject', $e->getError()->message);
          }



        $type_card = $charge->payment_method_details->card->brand;
        $last4 = $charge->payment_method_details->card->last4;
        $seller_message = $charge->outcome->seller_message;

        $pay = Pay::create([
            'uuid' => Str::uuid(),
            'id_stripe' => $charge->id,
            'amount_stripe' => $charge->amount,
            'amount_platform' => $package->price,
            'type_card' => $type_card,
            'status' => $charge->status,
            'last4' => $last4,
            'seller_message' => $seller_message,
            'receipt_url_stripe' => $charge->receipt_url,
            'idUser' => $user->id
        ]);

        if($pay->status == 'succeeded')
        {
            $date = Carbon::parse(date('Y-m-d'));
            $endDate = $date->addDay($package->daysDuration);

            $membresia = Memebresia::create([
                'uuid' => Str::uuid(),
                'idPackage' => $package->id,
                'idUser' => $user->id,
                'start' => date('Y-m-d'),
                'end' => $endDate,
                'idPay' => $pay->id
            ]);

            $user->idMembresia = $membresia->id;
            $user->save();

            return redirect()->route('detalleMembresia')->with('success', 'Membresia Pagada con exito');
        }
    }

    public function payReject($razon)
    {
        return view('pays.error', compact('razon'));
    }

    public function show($user)
    {
        $user = User::whereUuid($user)->first();
        $pays = Pay::where('idUser', $user->id)->get();

        return response()->json([
            'data' => $pays,
            'message' => 'Pagos Obtenidos con Exito',
            'status' => 200
        ]);
    }
}
