<?php

namespace App\Http\Controllers;


use App\Models\Templates;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TemplatesController extends Controller
{
    public function index()
    {
        $templates = Templates::withCount('messages')->get();

        return response()->json([
            'data' => $templates,
            'message' => 'Plantillas Obtenidas con Exito',
            'status' => 200
        ]);
    }

    public function store(Request $request)
    {
        $template = Templates::create([
            'uuid' => Str::uuid(),
            'title' => $request->title,
            'status' => 1,
        ]);

        return response()->json([
            'data' => $template,
            'message' => 'Plantilla Registrada con Exito',
            'status' => 200
        ]);
    }

    public function update(Request $request, $template)
    {
        $template = Templates::whereUuid($template)->first();
        $template->status = $request->status;
        $template->title = $request->title;
        $template->save();

        return response()->json([
            'data' => $template,
            'message' => 'Plantilla Actualizada con Exito',
            'status' => 200
        ]);
    }

    public function activar($template)
    {
        $template = Templates::whereUuid($template)->first();
        $template->status = 1;
        $template->save();

        return response()->json([
            'data' => $template,
            'message' => 'Plantilla Activada con Exito',
            'status' => 200
        ]);
    }

    public function desactivar($template)
    {
        $template = Templates::whereUuid($template)->first();
        $template->status = 2;
        $template->save();

        return response()->json([
            'data' => $template,
            'message' => 'Plantilla Desactivada con Exito',
            'status' => 200
        ]);
    }

    public function show($template)
    {
        $template = Templates::whereUuid($template)->first();

        return response()->json([
            'data' => $template,
            'message' => 'Plantilla Obtenida con Exito',
            'status' => 200
        ]);
    }

   
}
