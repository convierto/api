<?php
namespace App\Http\Controllers;

use App\Models\Paquetes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('auth.login');
})->name('raiz');

Auth::routes(["register" => false]);

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('register', [AuthController::class, 'register'])->name('register');
Route::post('register', [AuthController::class, 'store']);
Route::get('logout', [AuthController::class, 'logout'])->name('logoutx');
Route::get('edit-profile', [UserController::class, 'editProfile'])->name('editar-perfil');

Route::get('users', [UserController::class, 'index'])->name('users');
Route::post('user', [UserController::class, 'store'])->name('user');
Route::get('user-create', [UserController::class, 'create'])->name('create-user');
Route::get('user-edit/{user}', [UserController::class, 'edit'])->name('editUser');
Route::post('user-update/{user}', [UserController::class, 'update'])->name('updateUser');
Route::post('updateClave/{user}', [UserController::class, 'updateClave'])->name('updateClave');

Route::get('packages', [PaquetesController::class, 'index'])->name('packages');
Route::get('create-package', [PaquetesController::class, 'create'])->name('create-package');
Route::get('edit-package/{paquete}', [PaquetesController::class, 'edit'])->name('editPackage');
Route::post('package', [PaquetesController::class, 'store'])->name('package');
Route::post('update-package/{paquete}', [PaquetesController::class, 'update'])->name('updatePackage');


Route::get('config-chat-bot', [BotConfigController::class, 'index'])->name('configChatBot');
Route::post('updateChatBot/{config}', [BotConfigController::class, 'update'])->name('updateChatBot');


Route::get('hire-members', [PaquetesController::class, 'contratar'])->name('contratarMembresia');
Route::get('members-details', [MemebresiaController::class, 'show'])->name('detalleMembresia');
Route::get('upgrade-package', [PaquetesController::class, 'upgrade'])->name('upgrade-package');


Route::get('payment-package/{paquete}', [PayController::class, 'index'])->name('paymentPackage');
Route::get('generate-sesion-stripe/{paquete}', [PayController::class, 'generateSesionStripe'])->name('generateSesionStripe');


Route::post('payment_process', [PayController::class, 'processPayment']);

ROute::get('pays', [PayController::class, 'indexAdmin']);

Route::get('pay-reject/{razon}', [PayController::class, 'payReject'])->name('pay-reject');


ROute::get('pays', [PayController::class, 'payList'])->name('pays');
