@extends('layouts.app')

@section('contenido')

<div class="profile-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="edit-profile-content card-box-style">
                    <h3>Editar perfil</h3>

                    <form method="POST" action="{{ route('updateUser', $user->uuid) }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" placeholder="Tu nombre" name="name" value="{{ $user->name }}">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" class="form-control" placeholder="Tu apellido" name="lastName" value="{{ $user->lastName }}">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Correo</label>
                                    <input type="email" class="form-control" placeholder="Tu correo" name="email" value="{{ $user->email }}">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control" placeholder="Tu teléfono" name="phone" value="{{ $user->phone }}">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Sitio Web</label>
                                    <input type="text" class="form-control" placeholder="https://primeroleads.com" name="web" value="{{ $user->web }}">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Fecha de Nacimiento</label>
                                    <input type="date" name="date" class="form-control" id="">
                                </div>
                            </div>


                            <div class="save-update">
                                <button class="btn btn-primary me-2">Actualizar</button>
                                <a href="{{ route('users') }}" class="btn btn-danger">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="edit-profile-content card-box-style">
                    <h3>Cambiar contraseña</h3>
                    <form method="post" action="{{ route('updateClave', $user->uuid) }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Nueva contraseña</label>
                                    <input type="password" name="password" class="form-control" >
                                </div>
                            </div>

                            <div class="save-update">
                                <button class="btn btn-primary me-2">Actualizar</button>
                                <a href="{{ route('users') }}" class="btn btn-danger">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
