@extends('layouts.app')

@section('contenido')
<div class="page-title-area">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-9 col-sm-9">
                <div class="page-title">
                    <h3>Crear Usuario</h3>
                </div>
            </div>

            <div class="col-lg-3 col-sm-3">
                <a href="{{ route('users') }}" class="btn btn-warning ">Regresar</a>
            </div>
        </div>

        <div class="profile-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="edit-profile-content card-box-style">
                            <h3>Crear Usuario</h3>

                            <form method="POST" action="{{ route('user') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" class="form-control" placeholder="Tu nombre" name="name">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Apellido</label>
                                            <input type="text" class="form-control" placeholder="Tu apellido" name="lastName">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Correo</label>
                                            <input type="email" class="form-control" placeholder="Tu correo" name="email">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Teléfono</label>
                                            <input type="text" class="form-control" placeholder="Tu teléfono" name="phone">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Sitio Web</label>
                                            <input type="text" class="form-control" placeholder="https://primeroleads.com" name="web">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Clave</label>
                                            <input type="text" class="form-control" placeholder="*********" name="password">
                                        </div>
                                    </div>

                                    <div class="col-lg-12">

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Fecha de nacimiento</label>
                                                   <input type="date" name="date" class="form-control" id="">
                                                </div>
                                            </div>

                                          <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="">Tipo de Usuario</label>
                                                <select name="typeUser" class="form-control" id="">
                                                    <option value="">Seleccione</option>
                                                    <option value="2">Super Admin</option>
                                                    <option value="1">Usuario</option>
                                                </select>
                                            </div>
                                          </div>
                                        </div>
                                    </div>



                                    <div class="save-update">
                                        <button class="btn btn-primary me-2 w-100">Crear</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
