@extends('layouts.app')

@section('contenido')
    <div class="page-title-area">
        <div class="container-fluid">

            @if ($message = Session::get('success'))
            <div class="alert alert-success" role="alert">
                {{ $message }}
            </div>
            @endif


            <div class="row align-items-center">
                <div class="col-lg-9 col-sm-9">
                    <div class="page-title">
                        <h3>Usuarios</h3>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-3">
                    <a href="{{ route('create-user') }}" class="btn btn-success ">Crear Nuevo</a>
                </div>
            </div>

            <div class="default-table-area">
                <div class="container-fluid">
                    <div class="card-box-style">
                        <div class="others-title">
                            <h3>Default Table</h3>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Tipo Usuario</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <th scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->name }} {{ $user->lastName }}</td>
                                        <td>
                                            @if ($user->typeUser == 1)
                                                <span class="badge text-bg-info">Usuario</span>
                                            @else
                                                <span class="badge text-bg-success">Super Admin</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('editUser', $user->uuid) }}">Editar</a> ||
                                            <a href="">Eliminar</a>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>



                </div>
            </div>
        </div>
    </div>
@endsection
