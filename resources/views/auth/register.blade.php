<!doctype html>
<html lang="zxx">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Link Of CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/remixicon.css">
    <link rel="stylesheet" href="assets/css/boxicons.min.css">
    <link rel="stylesheet" href="assets/css/iconsax.css">
    <link rel="stylesheet" href="assets/css/metismenu.min.css">
    <link rel="stylesheet" href="assets/css/simplebar.min.css">
    <link rel="stylesheet" href="assets/css/calendar.css">
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css">
    <link rel="stylesheet" href="assets/css/jbox.all.min.css">
    <link rel="stylesheet" href="assets/css/editor.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/loaders.css">
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/sidebar-menu.css">
    <link rel="stylesheet" href="assets/css/footer.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/images/favicon.svg">
    <!-- Title -->
    <title>Convierto - Lead Chatbot</title>
</head>

<body class="body-bg-f8faff">

    <!-- Start Account Area -->
    <div class="account-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="account-content">
                        <div class="account-header">
                            <a href="index.html">
                                <img class="form-logo" src="assets/images/main-logo.svg" alt="main-logo">
                            </a>
                            <h3>Registro</h3>
                        </div>

                        <form method="POST" class="account-wrap" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group mb-24 icon">
                                <input type="text" class="form-control" placeholder="Nombre" name="name" required>
                                <img class="icon-img" src="assets/images/icon/user-square.svg" alt="user-square" name="name">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-24 icon">
                                <input type="text" class="form-control" placeholder="Apellido" name="lastName" required>
                                <img class="icon-img" src="assets/images/icon/user-square.svg" alt="user-square" name="lastName">
                            </div>
                            <div class="form-group mb-24 icon">
                                <input type="email" class="form-control" placeholder="Correo" name="email" required>
                                <img class="icon-img" src="assets/images/icon/sms.svg" alt="sms">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-24 icon">
                                <input type="password" class="form-control" placeholder="Contraseña" name="password" required>
                                <img class="icon-img" src="assets/images/icon/key.svg" alt="key">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-24">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value=""
                                        id="flexCheckDefault" required>
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Acepto <a href="terms.html">Términos y Condiciones.</a>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group mb-24">
                                <button type="submit" class="default-btn">Registrarme</button>
                            </div>
                            <div class="form-group mb-24">
                                <p class="account">¿Ya tienes una cuenta? <a href="login.html">Ingresar</a></p>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Account Area -->


    <!-- Start Go Top Area -->
    <div class="go-top">
        <i class="ri-arrow-up-s-fill"></i>
        <i class="ri-arrow-up-s-fill"></i>
    </div>
    <!-- End Go Top Area -->

    <!-- Jquery Min JS -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/metismenu.min.js"></script>
    <script src="assets/js/simplebar.min.js"></script>
    <script src="assets/js/geticons.js"></script>
    <script src="assets/js/sweetalert2.all.min.js"></script>
    <script src="assets/js/jbox.all.min.js"></script>
    <script src="assets/js/editor.js"></script>
    <script src="assets/js/form-validator.min.js"></script>
    <script src="assets/js/contact-form-script.js"></script>
    <script src="assets/js/ajaxchimp.min.js"></script>
</body>

</html>
