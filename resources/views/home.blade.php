@extends('layouts.app')

@section('contenido')
    <!-- Start Overview Back Area -->
    <div class="overview-content-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="single-audience d-flex justify-content-between align-items-center">
                        <div class="audience-content">
                            <h5>{{ $titulo1 }}</h5>
                            <h4>{{ $data1 }}</h4>
                        </div>
                        <div class="icon">
                            <img src="assets/images/icon/note-2.svg" alt="white-profile-2user">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6">
                    <div class="single-audience d-flex justify-content-between align-items-center">
                        <div class="audience-content">
                            <h5>{{ $titulo2 }}</h5>
                            <h4>{{ $data2 }}</h4>
                        </div>
                        <div class="icon">
                            <img src="assets/images/icon/user-2.svg" alt="eye">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Overview Back Area -->


    @if (Auth::user()->typeUser == 1)
    <!-- Start Contact List Area -->
                <div class="contact-list-area">
                    <div class="container-fluid">
                        <div class="table-responsive" data-simplebar>
                            <div class="row">
                                <div class="col-6 others-title">
                                    <h3>Listado de prospectos</h3>
                                </div>
                            </div>


                            <table class="table align-middle mb-0">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-check d-flex align-items-center">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label ms-2">
                                                    <img class="rounded-circle ms-3 me-3 border" src="assets/images/user/user-2.png" alt="user-2">
                                                </label>
                                                <div class="info">
                                                    <a href="single-perfil.html">
                                                        <h4>Zennilia Anderson</h4>
                                                    </a>
                                                    <a href="mailto:anderson@gmail.com">anderson@gmail.com</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="tel:+(124)45678910">+ (124) 456 78910</a>
                                        </td>
                                        <td>
                                            <span class="location">32/B, Central town, England</span>
                                        </td>
                                        <td>
                                            11/10/2023 - 18:00
                                        </td>

                                        <td>
                                            <ul class="d-flex justify-content-betweens">
                                                <li>
                                                    <a href="#">
                                                        <img src="assets/images/icon/call-2.svg" alt="call-2">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                        <img src="assets/images/icon/messages-2.svg" alt="messages-2">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="assets/images/icon/trash-2.svg" alt="trash-2">
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Contact List Area -->
    @endif
@endsection
