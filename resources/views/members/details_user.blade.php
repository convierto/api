@extends('layouts.app')

@section('contenido')
    @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            {{ $message }}
        </div>
    @endif

    <!-- Start Profile Area -->
    <div class="profile-area">
        <div class="container-fluid">
            <div class="profile-details card-box-style">
                <ul class="list-inline profile-menu">
                    <li>
                        <a href="" class="active">Detalle de Membresia Actual</a>
                    </li>
                </ul>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="personal-info">
                            <ul class="list-inline">
                                <li>
                                    <span>Nombre del Paquete :</span>
                                    {{ $membresia->detallePaquete->title }}
                                </li>
                                <li>
                                    <span>Duracion del Paquete :</span>
                                    {{ $membresia->detallePaquete->daysDuration }} Dias
                                </li>
                                <li>
                                    <span>Inicio :</span>
                                    {{ $membresia->start }}
                                </li>
                                <li>
                                    <span>Finalizacion :</span>
                                    {{ $membresia->end }}
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="personal-info">
                            <ul class="list-inline">
                                <li>
                                    <span>Detalle de Pago :</span>
                                    @if ($membresia->idPay == 0)
                                        FREE
                                    @else
                                        Pagado
                                    @endif
                                </li>
                                @if ($membresia->idPay != 0)
                                    <li>
                                        <span>Recibo de Stripe :</span>
                                        <a href="{{ $membresia->detallePago->receipt_url_stripe }}" target="_blank">Ver
                                            Recibo</a>
                                    </li>

                                    <li>
                                        <span>Datos Tarjeta :</span>
                                        {{ $membresia->detallePago->type_card }} / Termianda en:
                                        {{ $membresia->detallePago->last4 }}
                                    </li>
                                    <li>
                                        <span>Monto Pagado :</span>
                                        {{ $membresia->detallePago->amount_platform }}
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="biography">
                    <a href="{{ route('upgrade-package') }}" class="btn btn-success from-control">Mejorar Plan</a>
                </div>
            </div>


            <div class="default-table-area mt-3">
                <div class="container-fluid">
                    <div class="card-box-style">
                        <div class="others-title">
                            <h3>Todas tus membresias</h3>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Tipo de Paquete</th>
                                    <th scope="col">Inicio</th>
                                    <th scope="col">Fin</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($membresias != null)
                                    @foreach ($membresias as $membresia)
                                        <tr>
                                            <td>{{ $membresia->detallePaquete->title }}</td>
                                            <td> {{ $membresia->start }}</td>
                                            <td> {{ $membresia->end }}</td>
                                            {{--  <td>  <a href="{{ $membresia->detallePago->receipt_url_stripe }}" target="_blank">Descargar Recibo</a> </td>  --}}
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>



                </div>
            </div>
        </div>
    </div>
    <!-- End Profile Area -->
@endsection
