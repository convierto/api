@extends('layouts.app')

@section('contenido')
<!-- Start Pricing Area -->
<div class="pricing-area">
    <div class="container-fluid">
        <div class="card-box-style">
            <div class="row justify-content-center">
                @foreach ($paquetes as $paquete)
                <div class="col-lg-3 col-sm-6">
                    <div class="single-pricing">
                        <h3>{{ $paquete->title }}</h3>
                        <p>{{ $paquete->description }}</p>
                        <h1><sup>$</sup>{{ $paquete->price }}<sub>/ Por {{ $paquete->daysDuration }} dias</sub></h1>
                        <a href="{{ route('paymentPackage', $paquete->uuid) }}" class="default-btn">
                            Seleccionar Plan
                        </a>

                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- End Pricing Area -->
@endsection
