@extends('layouts.app')

@section('contenido')
    @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            {{ $message }}
        </div>
    @endif


    <!-- Start Chat Area -->
    <div class="chat-content-area mt-20">
        <div class="container-fluid">
            <div class="sidebar-left">
                <div class="sidebar">
                    <div class="edit-profile-content card-box-style">
                        <h3>Edita tu chatbot</h3>
                        <form method="POST" action="{{ route('updateChatBot', $config->uuid) }}">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nombre de tu Chat Bot.</label>
                                        <input type="text" class="form-control" id="name"
                                            value="{{ $config->name }}" onkeydown="changeName()" onkeyup="changeName()"
                                            onkeypress="changeName()" name="name">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Personaliza tu mensaje de bienvenida.</label>
                                        <input type="text" class="form-control" value="{{ $config->welcome_message }}"
                                            id="welcome" onkeydown="changeWelcome()" onkeyup="changeWelcome()"
                                            onkeypress="changeWelcome()" name="welcome_message">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Personaliza tu mensaje de despedida.</label>
                                        <input type="text" class="form-control" value="{{ $config->goodbye_message }}"
                                            id="goodbye" onkeydown="changeGoodBye()" onkeyup="changeGoodBye()"
                                            onkeypress="changeGoodBye()" name="goodbye_message">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Personaliza el estado de tu bot.</label>
                                        <select name="status" onchange="changeStatus()" class="form-control"
                                            id="status-sel" name="status">
                                            <option value="1" @if ($config->status == 1) selected @endif>
                                                Disponible</option>
                                            <option value="2" @if ($config->status == 2) selected @endif>No
                                                Disponible</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="save-update">
                                    <button class="btn btn-primary me-2">Actualizar</button>
                                    <a href="{{ route('home') }}" class="btn btn-danger">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="content-right">
                <div class="chat-area">
                    <div class="chat-list-wrapper">
                        <div class="chat-list">
                            <div class="chat-list-header d-flex align-items-center">
                                <div class="header-left d-flex align-items-center me-2">
                                    <div class="avatar me-2">
                                        <img src="assets/images/chat/chat-1.png" class="rounded-circle" alt="image">
                                        <span class="status-online"></span>
                                    </div>
                                    <div>
                                        <h6 class="mb-0 font-weight-bold" id="nameMu">{{ $config->name }}</h6>
                                        @if ($config->status == 1)
                                            <span id="status">Disponible</span>
                                        @else
                                            <span id="status">No Disponible</span>
                                        @endif

                                    </div>
                                </div>

                                <div class="header-right text-end w-100">
                                    <ul class="list-unstyled mb-0">
                                        <li>
                                            <div class="dropdown">
                                                <button class="dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                    <img src="assets/images/icon/dots.svg" alt="dots">
                                                </button>

                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                                        <i class="bx bx-log-in-circle"></i> Cerrar
                                                    </a>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="chat-container" data-simplebar>
                                <div class="chat-content">
                                    <div class="chat chat-left">
                                        <div class="chat-avatar">
                                            <a href="#" class="d-inline-block">
                                                <img src="assets/images/chat/chat-1.png" width="50" height="50"
                                                    class="rounded-circle" alt="image">
                                            </a>
                                        </div>

                                        <div class="chat-body">
                                            <div class="chat-message">
                                                <p id="welcome-chat">{{ $config->welcome_message }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chat">
                                        <div class="chat-avatar">
                                            <a href="#" class="d-inline-block">
                                                <img src="assets/images/chat/chat-6.png" width="50" height="50"
                                                    class="rounded-circle" alt="image">
                                            </a>
                                        </div>

                                        <div class="chat-body">
                                            <div class="chat-message">
                                                <p>Tengo dudas sobre tu servicio.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chat chat-left">
                                        <div class="chat-avatar">
                                            <a href="#" class="d-inline-block">
                                                <img src="assets/images/chat/chat-1.png" width="50" height="50"
                                                    class="rounded-circle" alt="image">
                                            </a>
                                        </div>

                                        <div class="chat-body">
                                            <div class="chat-message">
                                                <p id="goodbye-chat">{{ $config->goodbye_message }}</p>
                                            </div>
                                        </div>
                                    </div>





                                    <div class="chat">


                                        <div class="chat-body">
                                            <div class="chat-message">
                                                <p>Gracias!!</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="chat-list-footer">
                                <form class="d-flex align-items-center">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Escribe tu mensaje...">

                                    </div>

                                    <button type="submit" class="send-btn d-inline-block">
                                        <span>Enviar</span>
                                        <img src="assets/images/icon/send-2.svg" alt="send-2">
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Chat Area -->
@endsection


@section('scripts')
    <script>

        function changeName() {
            $("#nameMu").html($("#name").val())
        }

        function changeStatus() {
            if ($("#status-sel").val() == 1) {
                $("#status").html("Disponible")
            } else {
                $("#status").html("No Disponible")
            }
        }

        function changeWelcome() {
            $("#welcome-chat").html($("#welcome").val())
        }

        function changeGoodBye() {
            $("#goodbye-chat").html($("#goodbye").val())
        }
    </script>
@endsection
