<!DOCTYPE html>
<html lang="zxx">
    <head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Link Of CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/remixicon.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/boxicons.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/iconsax.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/metismenu.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/simplebar.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/calendar.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/sweetalert2.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/jbox.all.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/editor.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/loaders.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/header.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/sidebar-menu.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/footer.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.2.3/axios.min.js" integrity="sha512-L4lHq2JI/GoKsERT8KYa72iCwfSrKYWEyaBxzJeeITM9Lub5vlTj8tufqYk056exhjo2QDEipJrg6zen/DDtoQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

		<!-- Favicon -->
		<link rel="icon" type="image/png" href="assets/images/favicon.svg">
		<!-- Title -->
		<title>Convierto - Lead Chatbot</title>
    </head>

    <body class="body-bg-f8faff">

		<!-- Start All Section Area -->
        <div class="all-section-area">
            @include('layouts.header')
            @include('layouts.sidebar')

            <!-- Start Main Content Area -->
            <main class="main-content-wrap">
                @yield('contenido')

                <div class="flex-grow-1"></div>

                @include('layouts.footer')
            </main>
            <!-- End Main Content Area -->
        </div>
        <!-- End All Section Area -->

        <!-- Start Template Sidebar Area -->
        <div class="template-sidebar-area">
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight">
                <div class="offcanvas-header">
                    <a href="index.html">
                        <img src="assets/images/main-logo.svg" alt="main-logo">
                    </a>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul>
                        <li>
                            <a class="default-btn btn active" target="_blank" href="#">
                                Comprar ahora!
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- End Template Sidebar Area -->

		<!-- Start Go Top Area -->
		<div class="go-top">
			<i class="ri-arrow-up-s-fill"></i>
			<i class="ri-arrow-up-s-fill"></i>
		</div>
		<!-- End Go Top Area -->

        <!-- Jquery Min JS -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
        <script src="{{ asset('assets/js/simplebar.min.js') }}"></script>
        <script src="{{ asset('assets/js/geticons.js') }}"></script>
        <script src="{{ asset('assets/js/calendar.js') }}"></script>
        <script src="{{ asset('assets/js/editor.js') }}"></script>
		<script src="{{ asset('assets/js/form-validator.min.js') }}"></script>
		<script src="{{ asset('assets/js/contact-form-script.js') }}"></script>
		<script src="{{ asset('assets/js/ajaxchimp.min.js') }}"></script>
		<script src="{{ asset('assets/js/custom.js') }}"></script>

        @yield('scripts')
    </body>
</html>
