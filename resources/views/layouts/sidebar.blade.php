<!-- Start Sidebar Menu Area -->
<nav class="sidebar-menu">
    @if (Auth::user()->typeUser == 1)
        <ul class="list-group flex-column d-inline-block first-menu" data-simplebar>
            <li class="list-group-item main-grid active">
                <a href="#" class="icon">
                    <img src="{{ asset('assets/images/icon/element.svg') }}" alt="element">
                </a>
            </li>

            <li class="list-group-item main-grid">
                <a href="{{ route('configChatBot') }}" class="icon">
                    <img src="{{ asset('assets/images/icon/messages.svg') }}" alt="Chatbot">
                </a>
            </li>


            <li class="list-group-item main-grid">
                @if (Auth::user()->idMembresia != null)
                    <a href="{{ route('detalleMembresia') }}" class="icon">
                    @else
                        <a href="{{ route('contratarMembresia') }}" class="icon">
                @endif
                <img src="{{ asset('assets/images/icon/book.svg') }}" alt="profile-2user">
                </a>
            </li>
        </ul>
    @endif

    @if (Auth::user()->typeUser == 2)
        <ul class="list-group flex-column d-inline-block first-menu" data-simplebar>
            <li class="list-group-item main-grid active">
                <a href="#" class="icon">
                    <img src="{{ asset('assets/images/icon/element.svg') }}" alt="element">
                </a>
            </li>

            <li class="list-group-item main-grid">
                <a href="{{ route('users') }}" class="icon">
                    <img src="{{ asset('assets/images/icon/profile-2user.svg') }}" alt="element">
                </a>
            </li>

            <li class="list-group-item main-grid">
                <a href="{{ route('packages') }}" class="icon">
                    <img src="{{ asset('assets/images/icon/layer.svg') }}" alt="element">
                </a>
            </li>

            <li class="list-group-item main-grid">
                <a href="{{ route('pays') }}" class="icon">
                    <img src="{{ asset('assets/images/icon/layer.svg') }}" alt="element">
                </a>
            </li>
        </ul>
    @endif
</nav>
<!-- End Sidebar Menu Area -->
