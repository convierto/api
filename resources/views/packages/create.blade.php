@extends('layouts.app')

@section('contenido')
<div class="page-title-area">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-9 col-sm-9">
                <div class="page-title">
                    <h3>Crear Paquete</h3>
                </div>
            </div>

            <div class="col-lg-3 col-sm-3">
                <a href="{{ route('packages') }}" class="btn btn-warning ">Regresar</a>
            </div>
        </div>

        <div class="profile-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="edit-profile-content card-box-style">
                            <h3>Crear Paquete</h3>

                            <form method="POST" action="{{ route('package') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Titulo</label>
                                            <input type="text" class="form-control" placeholder="Titulo del Paquete" name="title" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Precio</label>
                                            <input type="text" class="form-control" placeholder="Precio del Paquete" name="price" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Descripcion</label>
                                            <input type="text" class="form-control" placeholder="Descripcion" name="description" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Duracion (Dias)</label>
                                            <input type="text" class="form-control" placeholder="Duracion" name="daysDuration" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Prueba</label>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" name="proof" value="1" id="customSwitches">
                                              </div>
                                        </div>
                                    </div>


                                    <div class="save-update">
                                        <button class="btn btn-primary me-2 w-100">Crear</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
