@extends('layouts.app')

@section('contenido')
    <div class="page-title-area">
        <div class="container-fluid">

            @if ($message = Session::get('success'))
                <div class="alert alert-success" role="alert">
                    {{ $message }}
                </div>
            @endif

            @if ($message = Session::get('danger'))
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
            @endif


            <div class="row align-items-center">
                <div class="col-lg-9 col-sm-9">
                    <div class="page-title">
                        <h3>Paquetes de Membresias</h3>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-3">
                    <a href="{{ route('create-package') }}" class="btn btn-success ">Crear Nuevo</a>
                </div>
            </div>

            <div class="default-table-area">
                <div class="container-fluid">
                    <div class="card-box-style">
                        <div class="others-title">
                            <h3>Default Table</h3>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Titulo</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Duracion (Dias)</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($paquetes as $paquete)
                                    <tr>
                                        <th scope="row">{{ $paquete->id }}</th>
                                        <td>{{ $paquete->title }} </td>
                                        <td>{{ $paquete->price }} </td>
                                        <td>{{ $paquete->daysDuration }} </td>
                                        <td>
                                            <a href="{{ route('editPackage', $paquete->uuid) }}">Editar</a> ||
                                            <a href="#"
                                                onclick="eliminar('{{ $paquete->uuid }}', '{{ $paquete->title }}')">Eliminar</a>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>



                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function eliminar(id, title) {
            Swal.fire({
                title: 'Esta Seguro?',
                text: "Seguro de que desea eliminar el paquete: " + title + "!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar!',
                cancelButtonText: 'Cancelar!'
            }).then((result) => {
                if (result.isConfirmed) {

                    axios.get('/api/delete-package/' + id).then((data) => {
                        console.log(data)

                        if(data.data == 200)
                        {
                            Swal.fire(
                            'Elimiando con exito!',
                            'Se ha elimiando el ressgitro.',
                            'success'
                        )

                        location.reload()
                        }

                    })


                }
            })
        }
    </script>
@endsection
