@extends('layouts.app')

@section('contenido')
    <div class="pricing-area">
        <div class="container-fluid">
            @if ( Session::has('flash_message') )
                <div class="alert alert-danger" role="alert">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <div class="card-box-style">
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="card-header bg-success text-white">
                                {{ $package->title }}
                            </div>
                            <div class="card-body">
                                <form action="/api/payment/{{ $package->uuid }}/{{ Auth::user()->uuid }}" method="POST">
                                    <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                        data-key="pk_test_51MRivTEigSsp7sX6SOloqiIIsWWOG9XAtQLCgx620MbHnK5lmaZRfMvHUpyo6Ot6k5PtTldSrJY1qbki704V8nfT00JLYsR63B"
                                        data-amount="{{ $package->price * 100 }}" data-name="Convierto Leads" data-description="{{ $package->title }}"
                                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png" data-locale="auto" data-currency="usd">
                                    </script>
                                </form>
                            </div>
                            <div class="card-footer">
                                Presiona el boton e inicia el proceso de pago!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

