@extends('layouts.app')


@section('contenido')
    <div class="pricing-area">
        <div class="container-fluid">
            <div class="alert alert-danger" role="alert">
                Su pago ha sido rechazado por la siguiente razon: {{ $razon }}
            </div>
        </div>
    </div>
@endsection
